---
layout: page
permalink: /guide/behaviour/
title: Behaviour settings
---

The *Behaviour* tab allows the user to set *per-instance* settings of the plugin.

![Behaviour settings](/assets/images/guide/behaviour/behaviour.png)


## MIDI

The **MIDI Input channel** parameter determines the channel that will be used as LibreArp's input. If it is set to
*Any*, notes on all channels will be used. If set to a channel number, only notes from that channel will be used and
notes on all other channels will be passed through.

The **MIDI Output channel** parameter determines the channel that will be used for notes that LibreArp generates.


## Note behaviour

When **Octave transposition** is enabled, notes that are outside of the *root octave* (see [Pattern
editor](../pattern_editor)) will be transposed. Otherwise they will just wrap around.

When **Smart octaves** is enabled, LibreArp will calculate the number of octaves spanned by the current input -
transposition will then be done by multiples of that number of octaves, ensuring that a higher LibreArp note will also
always be audibly higher (analogically for lower notes).

When **Use input velocity** is enabled, the velocity of the input notes will be multiplied by the velocity of LibreArp
notes. Otherwise only the LibreArp notes' velocities will be taken into account.

**Non-playing mode** determines the behaviour of the plugin when the host's playback is stopped:

* **Default (Global)** - Whatever is set in [Global settings](../global_settings)
* **Silence** - LibreArp plays nothing
* **Passthrough** - LibreArp passes all input notes through
* **Pattern** - LibreArp plays the drawn pattern


## Chord

The **Chord size** parameter allows the user to set a fixed number of input notes. If there are fewer actual input
notes, all excess LibreArp notes will be silent. If there are more actual input notes, LibreArp will pick the lowest or
the highest notes, based on the **Note selection mode**.


## Pattern offset

These two buttons serve to change the pattern playback offset. By default, the start of the pattern will be aligned with
the start of the transport (i.e. the start of a song in the host). Sometimes, this might be undesirable -- there may be
irregularities in the song that cause the pattern to be misaligned. Using the **Record offset** button, the alignment
may be changed.

To set the correct offset, follow these steps:

* While playback is stopped, click the **Record offset** button -- the text colour will change to
  <span style="color: red;">red</span> to indicate that the button is armed
* Set the host's transport to the position you want the pattern to start
* Start playback

This will change the offset. Notice that the **Record offset** button un-arms itself automatically.

To change the alignment back to the start of the transport, simply click the **Reset offset** button.


## Manual time signature

By default, LibreArp retrieves the current time signature from the host. Some hosts however do not provide this
information to its plugins. By checking **Enable** and using the controls in the **Manual time signature** section, the
time signature may be set to the desired value manually, instead of relying on the host.
