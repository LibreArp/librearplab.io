---
layout: page
permalink: /guide/setup/ardour/
title: Set up LibreArp with Ardour
---

* Create a new *MIDI Track*
* In the *Mixer* view, add *LibreArp* onto the MIDI track
* Move *LibreArp* above a synth in the track

![The MIDI track setup](/assets/images/guide/setup/ardour/ardour_setup.png)
