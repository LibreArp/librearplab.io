---
layout: page
permalink: /guide/setup/reaper/
title: Set up LibreArp with Reaper
---

* Create a new track using *Track / Insert Virtual Instrument on new track*
* In the window that opens, select *LibreArp* and click *Add*

![FX window with LibreArp selected](/assets/images/guide/setup/reaper/insert_virtual_instrument.png)

* Next, you need to add a synth that will play LibreArp's output
    * Make sure to place the synth **after** LibreArp in the chain

![Add synth](/assets/images/guide/setup/reaper/add_synth.png)

* Write some chords and play them via LibreArp

![Complete LibreArp setup in Reaper](/assets/images/guide/setup/reaper/reaper_setup.png)

### Additional notes

* For more advanced setups, you can place LibreArp on its own track and use the routing in Reaper to send MIDI from LibreArp to other tracks with synths on them
