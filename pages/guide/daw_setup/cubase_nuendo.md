---
layout: page
permalink: /guide/setup/cubasenuendo/
title: Set up LibreArp with Cubase/Nuendo
---

To use LibreArp in Cubase/Nuendo, you need two *Instrument Tracks* routed like this:

![The Routing Setup for LibreArp](/assets/images/guide/setup/cubasenuendo/cubasenuendo_routingPlan.png)

### Track 1: LibreArp

* Create a new instrument track using *Project / Add Track / Instrument*
* In the window that opens, select *LibreArp* as Instrument and click *Add Track*

![Adding an Instrument Track with LibreArp](/assets/images/guide/setup/cubasenuendo/cubasenuendo_addTrackWithLibreArp.png)

* It might be a good idea to name the track something like *LibreArp for MySynth*

![Renaming the Track](/assets/images/guide/setup/cubasenuendo/cubasenuendo_renameTrack.png)

### Track 2: Your Instrument

* Create another instrument track with the VST Instrument that should play LibreArps output (e.g. *MySynth*)
* In the *Inspector*, set the *MIDI Input* of the track to the MIDI Output of LibreArp (e.g. *LibreArp - MIDI Output*)
    * Note that MIDI outputs are always named after their respective source tracks, so, if you have multiple LibreArp instances, make sure to give them proper names in order to be able to distinguish them
* Enable Monitoring on the instrument track with the synth

![Input set to Output of LibreArp, Monitoring Enabled](/assets/images/guide/setup/cubasenuendo/cubasenuendo_inputSetup.png)

### Playing/Recording

* Playing/Recording is done on Track 1 *LibreArp for MySynth*

![Playing and Recording](/assets/images/guide/setup/cubasenuendo/cubasenuendo_playingAndRecording.png)


### Additional notes

* **Warning:** If you play the patterns on an existing recording: know your MIDI-Panic-Buttons, since pausing mid playback can sometimes result in hanging notes (probably something with non-matching Note-Off messages due to the processing). Playing a whole MIDI sequence until all notes are off again is fine however.
* Since LibreArp's output shows up as a new *MIDI input*, multiple tracks can be configured to receive the same arpeggiated output.
    * This also allows more complex setups, like chaining two LibreArps with different cycle times for more generic control. And then having one VST play the slower arpeggio, while another plays the faster arpeggiated arpeggio.
* If you want a MIDI version with the arpeggiation printed in, you may record-enable *MySynth* too. As long as the *LibreArp for MySynth* track has a recording, you can always change the arpeggiation sequence of the existing recording and 'reprint' it by playing Track 1 *LibreArp for MySynth* while recording Track 2 *MySynth*.

![Printing the Arpeggiated Version of the Original Recording to MIDI](/assets/images/guide/setup/cubasenuendo/cubasenuendo_printingToMidi.png)

