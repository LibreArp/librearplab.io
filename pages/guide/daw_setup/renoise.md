---
layout: page
permalink: /guide/setup/renoise/
title: Set up LibreArp with Renoise
---

* Add *LibreArp* as a plugin instrument
* On the *Plugin* tab, set *MIDI Routing* to your target instrument you wish to arpeggiate
* Play chords via *LibreArp*

![Renoise complete setup](/assets/images/guide/setup/renoise/renoise_setup.png)
