---
layout: page
permalink: /guide/setup/fl/
title: Set up LibreArp with FL Studio
---

* Create a new *Patcher* channel
* Add *LibreArp* in *Patcher*
* Double-click the plugin in *Patcher*
    * An editor window should show up
* Click the cogwheel in the top-left corner of the editor window

![Cogwheel in the top-left corner of the editor window](/assets/images/guide/setup/fl/patcher_cogwheel.png)

* In the **MIDI** section set the **Input port** and **Output port** to **0**
    * The plugin should now have MIDI input and output nodes in *Patcher*

![MIDI ports](/assets/images/guide/setup/fl/patcher_midi_ports.png)

* Connect the input node to **From FL Studio**
* Add a synth to the *Patcher* (e.g. the built-in *Sytrus*)
* Disconnect the synth's input node from the **From FL Studio** and connect it
  to the output node of *LibreArp*
* Connect the synth's output node to **To FL Studio** (if it is not already)

![Patcher complete setup](/assets/images/guide/setup/fl/patcher_setup.png)
