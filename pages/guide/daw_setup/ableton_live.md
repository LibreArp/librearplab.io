---
layout: page
permalink: /guide/setup/live/
title: Set up LibreArp with Ableton Live
---

* Create a new *MIDI Track*
    * For better distinction, name the track, e.g. 'LibreArp'
* Add *LibreArp* onto this newly created track
* Create another *MIDI Track*
* Set this newly created track's *Monitor* to *In*
* Add a synth onto this newly created track
* Set this track to receive post-FX MIDI from the MIDI track with *LibreArp*

![*MIDI From* setting](/assets/images/guide/setup/live/live_midi_from.png)
