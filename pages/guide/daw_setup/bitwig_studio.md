---
layout: page
permalink: /guide/setup/bitwig/
title: Set up LibreArp with Bitwig Studio
---

* Create an *Instrument track*
* Add *LibreArp* to the track
* Add a synth (e.g. the built-in *Polysynth*) to the instrument track after
  *LibreArp*

**Note:** Due to VST limitations, LibreArp is marked as an instrument plugin. Because of this, Bitwig will automatically filter devices in the device selection window and may not display more when LibreArp is already present. You may simply change the filter in the bottom-left of the device selection window.

![Bitwig setup](/assets/images/guide/setup/bitwig/bitwig_setup.png)
